﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Models.ViewModels;
using HospitalProject.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Data.SqlClient;

// Based off Christine's Example

namespace HospitalProject.Controllers
{
    public class FaqController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public FaqController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        // Return the list view for FAQs
        public async Task<ActionResult> Index(int pageNum)
        {
            var user = await GetCurrentUserAsync();

            // From Christine's Pagination Notes
            var _faqs = await db.Faqs.Include(f => f.Admin).Include(f => f.Category).ToListAsync();
            int faqCount = _faqs.Count();
            int perPage = 5;
            int maxPage = (int)Math.Ceiling((decimal)faqCount / perPage) - 1;
            if (maxPage < 0) maxPage = 0;
            if (pageNum < 0) pageNum = 0;
            if (pageNum > maxPage) pageNum = maxPage;
            int start = perPage * pageNum;

            ViewData["pageNum"] = (int)pageNum;
            ViewData["paginationSummary"] = "";

            if (maxPage > 0)
            {
                ViewData["paginationSummary"] = (pageNum + 1).ToString() + " of " + (maxPage + 1).ToString();
            }

            List<Faq> faqs = await db.Faqs.Include(f => f.Category).Skip(start).Take(perPage).ToListAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            return View(faqs);
        }

        // Return the Create view for FAQs
        public ActionResult Create()
        {
            return View(db.Categories.ToList());
        }

        // Runs after the form to create an FAQ is submitted
        // Create the new entry in the FAQ table and return the list view
        [HttpPost]
        public async Task<ActionResult> Create(string faq_question, string faq_answer, string faq_category)
        {
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID == null) return Forbid(); //forbid because no admin account

            string queryString = "INSERT INTO faqs (Question, Answer, DateAdded, CategoryID, AdminID) " +
                "VALUES (@question, @answer, @dateadded, @categoryid, @adminid)";

            SqlParameter[] queryParams = new SqlParameter[5];
            queryParams[0] = new SqlParameter("@question", faq_question);
            queryParams[1] = new SqlParameter("@answer", faq_answer);
            queryParams[2] = new SqlParameter("@dateadded", DateTime.Now);
            queryParams[3] = new SqlParameter("@categoryid", int.Parse(faq_category));
            queryParams[4] = new SqlParameter("@adminid", user.AdminID);

            db.Database.ExecuteSqlCommand(queryString, queryParams);

            return RedirectToAction("Index");
        }

        // Return the Edit View for a particular FAQ
        public async Task<ActionResult> Edit(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }
            Faq faq = db.Faqs.Find(id);
            FaqEdit faqEditView = new FaqEdit();

            if (faq == null)
            {
                return NotFound();
            }

            faqEditView.Faq = faq;
            faqEditView.Categories = db.Categories.ToList();

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they have no account
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they have the wrong account
            }
            return View(faqEditView);
        }

        // Runs after the form to edit a particular FAQ is submitted
        // Update the particular FAQ in the Faqs table and return the list view
        [HttpPost]
        public async Task<ActionResult> Edit(int? id, string faq_question, string faq_answer, string faq_category)
        {
            if ((id == null) || (db.Faqs.Find(id) == null))
            {
                return new StatusCodeResult(400);
            }

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID == null) return Forbid(); //forbid because no admin account

            string queryString = "UPDATE faqs SET Question=@question, Answer=@answer, CategoryID=@categoryid, " +
                "AdminID=@adminid WHERE FaqID=@faqid";

            SqlParameter[] queryParams = new SqlParameter[6];
            queryParams[0] = new SqlParameter("@question", faq_question);
            queryParams[1] = new SqlParameter("@answer", faq_answer);
            queryParams[2] = new SqlParameter("@categoryid", int.Parse(faq_category));
            queryParams[3] = new SqlParameter("@adminid", user.AdminID);
            queryParams[4] = new SqlParameter("@faqid", id);

            db.Database.ExecuteSqlCommand(queryString, queryParams);

            return RedirectToAction("Index");
        }

        // Return the Delete view for a particular FAQ
        public async Task<ActionResult> Delete(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }

            Faq faq = await db.Faqs.Include(f => f.Category).SingleOrDefaultAsync(f => f.FaqID == id);
            if (faq == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); // forbid because they haven't logged in
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they are are the wrong person
            }
            return View(faq);
        }

        // Runs after the Delete is confirmed
        // Remove the entry from the Faqs table and return the list view
        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed(int id, int admin_id)
        {
            Faq faq = await db.Faqs.FindAsync(id);

            var user = await GetCurrentUserAsync();
            if (user.AdminID != admin_id)
            {
                return Forbid();
            }

            db.Faqs.Remove(faq);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}