﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using MimeKit;


namespace HospitalProject.Controllers
{
    public class CreateNewsLetterController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public CreateNewsLetterController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }
        public IActionResult Index()
        {
            return Create();
        }
        public ActionResult Create()
        {
            return View();
        }
        public async Task<ActionResult> List(int pagenum)
        {
            var user = await GetCurrentUserAsync();         
            var _newsletters = await db.Newsletter.Include(f => f.Admin).ToListAsync();
            // var _newsletters = db.Newsletter.ToList();
            int NewsettersCount = _newsletters.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)NewsettersCount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] = (pagenum + 1).ToString() + " of " + (maxpage + 1).ToString();
            }
            List<Newsletter> newsletters = db.Newsletter.Skip(start).Take(perpage).ToList();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }
            return View(newsletters);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("name, post_details")] Newsletter newsletter)
        {
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID != newsletter.AdminID)
            {
                return Forbid();//forbid because they have the wrong account
            }
            if (ModelState.IsValid)
            {
                newsletter.date_created = DateTime.Now;
                db.Newsletter.Add(newsletter);
                db.SaveChanges();
                List<Newsletter> n = db.Newsletter.ToList();
                int id = (int)n.Max(newl => newl.id);
                Newsletter nl = db.Newsletter.Find(id);
                string msg = "New newsleter from Humber Hospital </br>Date: " + nl.date_created + "</br>" +nl.name + "</br>" + nl.post_details;
                List<NewsletterSubscribers> ns = db.NewsletterSubscribers.ToList();

                foreach (var nsu in ns)
                {


                    var m = new MimeMessage();
                    m.From.Add(new MailboxAddress("admin", "jphospital98887@gmail.com"));
                    m.To.Add(new MailboxAddress(nsu.name, nsu.email));
                    m.Subject = " New Newsletter from humber hospital";
                    m.Body = new TextPart("html")
                    {
                        Text = msg
                    };


                    using (var client = new MailKit.Net.Smtp.SmtpClient())
                    {
                        client.Connect("smtp.gmail.com", 587, false);
                        client.Authenticate("jphospital98887@gmail.com", "Abc1234@");
                        client.Send(m);
                        client.Disconnect(true);
                    }
                }

                //return RedirectToAction("Sendletter/" + id);
                return RedirectToAction("List");
            }
            return View(newsletter);
        }
    }
}