﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Models.ViewModels;
using HospitalProject.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;


namespace HospitalProject.Controllers
{
    public class PatientProfileController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public PatientProfileController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        public ActionResult Index()
        {

            return RedirectToAction("List");
        }

        public async Task<ActionResult> List(int pagenum)
        {
            var _profiles = await db.PatientProfiles.Include(a=> a.Admin).ToListAsync();
            int profilecount = _profiles.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)profilecount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] = (pagenum + 1).ToString() + " of " + (maxpage + 1).ToString();

            }
            List<PatientProfile> patientProfiles = await db.PatientProfiles.Include(a=> a.Admin).Skip(start).Take(perpage).ToListAsync();
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(await db.PatientProfiles.ToListAsync());
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(await db.PatientProfiles.ToListAsync());
            }
        }

        public ActionResult Create()
        {
            PatientProfileEdit patientProfileEdit = new PatientProfileEdit();

            patientProfileEdit.Departments = db.Departments.ToList();
         

            return View(patientProfileEdit);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("FirstName, LastName, DOB, Notes, DepartmentID")] PatientProfile patientProfile)
        {
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); // forbid becasue no user is logged in
            if (user.AdminID == null) return Forbid(); // because admin account is not logged in
            if (ModelState.IsValid)
            {
                patientProfile.AdminID = user.AdminID.GetValueOrDefault();
                patientProfile.Admin = user.Admin;
                db.PatientProfiles.Add(patientProfile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(patientProfile);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind("PatientID,AdminID,FirstName,LastName,DOB,Notes,DepartmentID")] PatientProfile patientProfile)
        {
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID != patientProfile.AdminID)
            {
                return Forbid();//forbid because they have the wrong account
            }
            if (ModelState.IsValid)
            {
                db.Entry(patientProfile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(patientProfile);
        }

        public async Task<ActionResult> Edit(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }
            PatientProfile patientProfile = db.PatientProfiles.Find(id);
            PatientProfileEdit pfEditView = new PatientProfileEdit();
            if (patientProfile == null)
            {
                return NotFound();
            }
            pfEditView.PatientProfile = patientProfile;
            pfEditView.Departments = db.Departments.ToList();

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they have no account
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they have the wrong account
            }
            return View(pfEditView);
        }

        public async Task<ActionResult> Details(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }
            PatientProfile patientProfile = db.PatientProfiles.Find(id);
            if (patientProfile == null)
            {
                return NotFound();
            }

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid();
            if (user.AdminID != adminid)
            {
                return Forbid();
            }
            return View(patientProfile);
        }

        public async Task<ActionResult> Delete(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }

            PatientProfile patientProfile = await db.PatientProfiles.SingleOrDefaultAsync(p => p.PatientID == id);
            if (patientProfile == null)
            {
                return NotFound();
            }

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); // forbid because they haven't logged in
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they are are the wrong person
            }
            return View(patientProfile);
        }

        /* This action is to delete a country from the Country Table after pressing submit button
         and return to Country/Index
        */
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id, int adminid)
        {
            PatientProfile patientProfile = await db.PatientProfiles.FindAsync(id);

            var user = await GetCurrentUserAsync();
            if (user.AdminID != adminid)
            {
                return Forbid();
            }
            db.PatientProfiles.Remove(patientProfile);
            db.SaveChanges();
            return RedirectToAction("Index"); // To go back to Country/Index 

        }
    }
}