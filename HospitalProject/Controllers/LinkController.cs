﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Data;
using HospitalProject.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;

namespace HospitalProject.Controllers
{
    public class LinkController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public LinkController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
             db = context;
            _env = env;
            _userManager = usermanager;
        }

        // GET: Link
        public async Task<IActionResult> Index(int pageNum)
        {
            var user = await GetCurrentUserAsync();

            var _links = await db.Links.ToListAsync();
            int linkCount = _links.Count();
            int perPage = 5;
            int maxPage = (int)Math.Ceiling((decimal)linkCount / perPage) - 1;
            if (maxPage < 0) maxPage = 0;
            if (pageNum < 0) pageNum = 0;
            if (pageNum > maxPage) pageNum = maxPage;
            int start = perPage * pageNum;

            ViewData["pageNum"] = (int)pageNum;
            ViewData["paginationSummary"] = "";

            if (maxPage > 0)
            {
                ViewData["paginationSummary"] = (pageNum + 1).ToString() + " of " + (maxPage + 1).ToString();
            }

            List<Link> links = await db.Links.Skip(start).Take(perPage).ToListAsync();


            if (user != null)
            {
                if (user.AdminID == null)
                {
                    ViewData["UserHasAdmin"] = "False";
                }
                else
                {
                    ViewData["UserHasAdmin"] = user.AdminID.ToString();
                }
                return View(links);
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(links);
            }
        }

        // GET: Link/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var link = await db.Links
                .Include(l => l.page)
                .SingleOrDefaultAsync(m => m.LinkID == id);
            if (link == null)
            {
                return NotFound();
            }

            return View(link);
        }

        // GET: Link/Create
        public IActionResult Create()
        {
            Link link = new Link();
            link.LinkCreatedDate = DateTime.Today;
            link.LinkUpdatedDate = DateTime.Today;
            ViewData["PageID"] = new SelectList(db.Pages, "PageID", "PageTitle");
            return View(link);
        }

        // POST: Link/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("LinkID,LinkText,LinkType,LinkExternalURL,LinkCreatedDate,LinkUpdatedDate,IsPublished,IsDeleted,PageID")] Link link)
        {
            if (ModelState.IsValid)
            {
                db.Add(link);
                await db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PageID"] = new SelectList(db.Pages, "PageID", "PageContent", link.PageID);
            return View("Error");
        }

        // GET: Link/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var link = await db.Links.SingleOrDefaultAsync(m => m.LinkID == id);
            if (link == null)
            {
                return NotFound();
            }
            ViewData["PageID"] = new SelectList(db.Pages, "PageID", "PageContent", link.PageID);
            return View(link);
        }

        // POST: Link/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("LinkID,LinkText,LinkType,LinkExternalURL,LinkCreatedDate,LinkUpdatedDate,IsPublished,IsDeleted,PageID")] Link link)
        {
            if (id != link.LinkID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Update(link);
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LinkExists(link.LinkID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PageID"] = new SelectList(db.Pages, "PageID", "PageContent", link.PageID);
            return View(link);
        }

        // GET: Link/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var link = await db.Links
                .Include(l => l.page)
                .SingleOrDefaultAsync(m => m.LinkID == id);
            if (link == null)
            {
                return NotFound();
            }

            return View(link);
        }

        // POST: Link/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var link = await db.Links.SingleOrDefaultAsync(m => m.LinkID == id);
            db.Links.Remove(link);
            await db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LinkExists(int id)
        {
            return db.Links.Any(e => e.LinkID == id);
        }
    }
}
