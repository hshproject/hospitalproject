﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using HospitalProject.Models;
using HospitalProject.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

// Based off Christine's Example

namespace HospitalProject.Controllers
{
    public class CategoryController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public CategoryController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        // Return the List View for Categories
        public async Task<ActionResult> Index(int pageNum)
        {
            var user = await GetCurrentUserAsync();

            // From Christine's Pagination Notes
            var _categories = await db.Categories.Include(c => c.Admin).ToListAsync();
            int categoryCount = _categories.Count();
            int perPage = 5;
            int maxPage = (int)Math.Ceiling((decimal)categoryCount / perPage) - 1;
            if (maxPage < 0) maxPage = 0;
            if (pageNum < 0) pageNum = 0;
            if (pageNum > maxPage) pageNum = maxPage;
            int start = perPage * pageNum;

            ViewData["pageNum"] = (int)pageNum;
            ViewData["paginationSummary"] = "";

            if (maxPage > 0)
            {
                ViewData["paginationSummary"] = (pageNum + 1).ToString() + " of " + (maxPage + 1).ToString();
            }

            List<Category> categories = await db.Categories.Include(c => c.Admin).Skip(start).Take(perPage).ToListAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            return View(categories);
        }

        // Return the Create View for Categories
        public ActionResult Create()
        {
            return View();
        }

        // Runs after the Create form for Categories is submitted
        // Create a new category and return the list view
        [HttpPost]
        public async Task<ActionResult> Create(string category_name)
        {
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID == null) return Forbid(); //forbid because no admin account

            string queryString = "INSERT INTO categories (Name, AdminID) VALUES (@name, @adminid)";

            SqlParameter[] queryParams = new SqlParameter[2];
            queryParams[0] = new SqlParameter("@name", category_name);
            queryParams[1] = new SqlParameter("@adminid", user.AdminID);

            db.Database.ExecuteSqlCommand(queryString, queryParams);

            return RedirectToAction("Index");
        }

        // Return the Edit View for a particular Category
        public async Task<ActionResult> Edit(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return NotFound();
            }

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they have no account
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they have the wrong account
            }
            return View(category);
        }

        // Runs after the edit form for a category is submitted
        // Update the respective entry in the categories table and return the list view
        [HttpPost]
        public async Task<ActionResult> Edit(int? id, string category_name)
        {
            if ((id == null) || (db.Categories.Find(id) == null))
            {
                return new StatusCodeResult(400);
            }

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID == null) return Forbid(); //forbid because no admin account

            string queryString = "UPDATE categories SET Name=@name, AdminID=@adminid WHERE CategoryID=@categoryid";

            SqlParameter[] queryParams = new SqlParameter[3];
            queryParams[0] = new SqlParameter("@name", category_name);
            queryParams[1] = new SqlParameter("@adminid", user.AdminID);
            queryParams[2] = new SqlParameter("@categoryid", id);

            db.Database.ExecuteSqlCommand(queryString, queryParams);

            return RedirectToAction("Index");
        }

        // Return the Delete View for a particular category
        public async Task<ActionResult> Delete(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }

            Category category = await db.Categories.SingleOrDefaultAsync(c => c.CategoryID == id);
            if (category == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); // forbid because they haven't logged in
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they are are the wrong person
            }
            return View(category);
        }

        // Runs after a delete is confirmed
        // Delete all faqs that use this category and the particular entry from the categories table
        // Return the list view for categories
        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed(int id, int admin_id)
        {
            Category category = await db.Categories.FindAsync(id);
            // Referenced https://stackoverflow.com/questions/23090459/ienumerable-where-and-tolist-what-do-they-really-do
            // Get all FAQs that have this CategoryID
            IEnumerable<Faq> faqsToDelete = db.Faqs.ToList().Where(faq => faq.CategoryID.Equals(category.CategoryID));

            var user = await GetCurrentUserAsync();
            if (user.AdminID != admin_id)
            {
                return Forbid();
            }

            db.Faqs.RemoveRange(faqsToDelete);
            db.Categories.Remove(category);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}