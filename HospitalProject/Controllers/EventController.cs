﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Models.ViewModels;
using HospitalProject.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using System.Data.SqlClient;

// Based off Christine's Example

namespace HospitalProject.Controllers
{
    public class EventController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public EventController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        // Return the List View for Events
        public async Task<ActionResult> Index(int pageNum)
        {
            var user = await GetCurrentUserAsync();

            // From Christine's Pagination Notes
            var _events = await db.Events.Include(e => e.Admin).ToListAsync();
            int eventCount = _events.Count();
            int perPage = 5;
            int maxPage = (int)Math.Ceiling((decimal)eventCount / perPage) - 1;
            if (maxPage < 0) maxPage = 0;
            if (pageNum < 0) pageNum = 0;
            if (pageNum > maxPage) pageNum = maxPage;
            int start = perPage * pageNum;

            ViewData["pageNum"] = (int)pageNum;
            ViewData["paginationSummary"] = "";

            if (maxPage > 0)
            {
                ViewData["paginationSummary"] = (pageNum + 1).ToString() + " of " + (maxPage + 1).ToString();
            }

            List<Event> events = await db.Events.Include(e => e.Admin).Skip(start).Take(perPage).ToListAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            return View(events);
        }

        // Return the Details view for a particular Event
        public async Task<ActionResult> Details(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }
            Event hospEvent = db.Events.Find(id);

            if (hospEvent == null)
            {
                return NotFound();
            }

            var user = await GetCurrentUserAsync();

            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            return View(hospEvent);
        }

        // Return the Create View for Events
        public ActionResult Create()
        {
            return View();
        }

        // Runs after the form to create a new Event is submitted
        // Add an entry to the events table and return the list view
        [HttpPost]
        public async Task<ActionResult> Create(string event_name, string event_desc,
            string event_start_date, string event_start_time, string event_end_date, string event_end_time)
        {
            string startDate = event_start_date + " " + event_start_time;
            string endDate = event_end_date + " " + event_end_time;

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID == null) return Forbid(); //forbid because no admin account

            string queryString = "INSERT INTO events (Name, Description, StartTime, EndTime, AdminID) " +
                "VALUES (@name, @desc, @starttime, @endtime, @adminid)";

            SqlParameter[] queryParams = new SqlParameter[5];
            queryParams[0] = new SqlParameter("@name", event_name);
            queryParams[1] = new SqlParameter("@desc", event_desc);
            queryParams[2] = new SqlParameter("@starttime", DateTime.Parse(startDate));
            queryParams[3] = new SqlParameter("@endtime", DateTime.Parse(endDate));
            queryParams[4] = new SqlParameter("@adminid", user.AdminID);

            db.Database.ExecuteSqlCommand(queryString, queryParams);

            return RedirectToAction("Index");
        }

        // Return the edit view for a particular Event
        public async Task<ActionResult> Edit(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }
            Event hospEvent = db.Events.Find(id);

            if (hospEvent == null)
            {
                return NotFound();
            }

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they have no account
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they have the wrong account
            }
            return View(hospEvent);
        }

        // Runs after the form to edit an event is submitted
        // Update the entry for the particular event in the events table and return the details view for the specific event
        [HttpPost]
        public async Task<ActionResult> Edit(int? id, string event_name, string event_desc,
            string event_start_date, string event_start_time, string event_end_date, string event_end_time)
        {
            if ((id == null) || (db.Events.Find(id) == null))
            {
                return new StatusCodeResult(400);
            }

            string startDate = event_start_date + " " + event_start_time;
            string endDate = event_end_date + " " + event_end_time;

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID == null) return Forbid(); //forbid because no admin account

            string queryString = "UPDATE events SET Name=@name, Description=@desc, StartTime=@starttime, " +
                "EndTime=@endtime, AdminID=@adminid WHERE EventId=@eventid";

            SqlParameter[] queryParams = new SqlParameter[6];
            queryParams[0] = new SqlParameter("@name", event_name);
            queryParams[1] = new SqlParameter("@desc", event_desc);
            queryParams[2] = new SqlParameter("@starttime", DateTime.Parse(startDate));
            queryParams[3] = new SqlParameter("@endtime", DateTime.Parse(endDate));
            queryParams[4] = new SqlParameter("@adminid", user.AdminID);
            queryParams[5] = new SqlParameter("@eventid", id);

            db.Database.ExecuteSqlCommand(queryString, queryParams);

            return RedirectToAction("Details", new { id, adminid = user.AdminID });
        }

        // Return the Delete view for a particular event
        public async Task<ActionResult> Delete(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }

            Event hospEvent = await db.Events.SingleOrDefaultAsync(e => e.EventID == id);
            if (hospEvent == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); // forbid because they haven't logged in
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they are are the wrong person
            }
            return View(hospEvent);
        }

        // Runs after a delete is confirmed
        // Return the List View for Events
        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed(int id, int admin_id)
        {
            Event hospEvent = await db.Events.FindAsync(id);

            var user = await GetCurrentUserAsync();
            if (user.AdminID != admin_id)
            {
                return Forbid();
            }

            db.Events.Remove(hospEvent);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}