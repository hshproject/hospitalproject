﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Models.ViewModels;
using HospitalProject.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using MimeKit;

namespace HospitalProject.Controllers
{
    public class NewsletterController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public NewsletterController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }
        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> List(int pagenum)
        {
            var user = await GetCurrentUserAsync();
            var _newsletterSubscribers = await db.NewsletterSubscribers.Include(f => f.Admin).ToListAsync();
            // var _newsletterSubscribers = db.NewsletterSubscribers.ToList();
            int SubscribersCount = _newsletterSubscribers.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)SubscribersCount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] = (pagenum + 1).ToString() + " of " + (maxpage + 1).ToString();
            }
            List<NewsletterSubscribers> newsletterSubscribers = db.NewsletterSubscribers.Skip(start).Take(perpage).ToList();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            return View(newsletterSubscribers);
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("name, email")] NewsletterSubscribers newsletterSubscribers)
        {
            if (ModelState.IsValid)
            {
                newsletterSubscribers.AdminID = 1;
                db.NewsletterSubscribers.Add(newsletterSubscribers);
                db.SaveChanges();                
                List<NewsletterSubscribers> n = db.NewsletterSubscribers.ToList();
                int id = (int)n.Max(newl => newl.id);
                NewsletterSubscribers nl = db.NewsletterSubscribers.Find(id);
                var m = new MimeMessage();
                m.From.Add(new MailboxAddress("admin", "jphospital98887@gmail.com"));
                m.To.Add(new MailboxAddress(nl.name, nl.email));
                m.Subject = " New Newsletter from humber hospital";
                m.Body = new TextPart("html")
                {
                    Text = nl.name + ", Thank you for subcribing to our NewsLetter</br>***********************</br> Now you will start recieveing our Newsletters"
                };


                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.Connect("smtp.gmail.com", 587, false);
                    client.Authenticate("wdn01269796@gmail.com", "mailtest1234");
                    client.Send(m);
                    client.Disconnect(true);
                }
                return RedirectToAction("List");
            }
            return View(newsletterSubscribers);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind("id,name, email")] NewsletterSubscribers newsletterSubscribers)
        {

            if (ModelState.IsValid)
            {
                db.Entry(newsletterSubscribers).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(newsletterSubscribers);
        }


        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            NewsletterSubscribers newsletterSubscribers = db.NewsletterSubscribers.Find(id);
            if (newsletterSubscribers == null)
            {
                return NotFound();
            }
            return View(newsletterSubscribers);
        }


        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            NewsletterSubscribers newsletterSubscribers = db.NewsletterSubscribers.Find(id);
            if (newsletterSubscribers == null)
            {
                return NotFound();
            }
            return View(newsletterSubscribers);
        }



        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            NewsletterSubscribers newsletterSubscribers = db.NewsletterSubscribers.Find(id);
            if (newsletterSubscribers == null)
            {
                return NotFound();
            }

            return View(newsletterSubscribers);
        }

        //for popping up the confirm delete page
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NewsletterSubscribers newsletterSubscribers = db.NewsletterSubscribers.Find(id);
            db.NewsletterSubscribers.Remove(newsletterSubscribers);
            db.SaveChanges();
            return RedirectToAction("Index");

        }


    }
}