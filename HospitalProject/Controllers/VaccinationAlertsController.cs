﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Models.ViewModels;
using HospitalProject.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Net;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IO;



namespace HospitalProject.Controllers
{
    public class VaccinationAlertsController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public VaccinationAlertsController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }
        // GET: VaccinationAlerts
        //public async Task<IActionResult> Index()
        //{
        //    var hospitalDbContext = db.Vaccination_Alerts.Include(v => v.Admin);
        //    return View(await hospitalDbContext.ToListAsync());
        //}

        public async Task<ActionResult> Index(int pagenum)
        {
            var user = await GetCurrentUserAsync();
            var Appointment = await db.Vaccination_Alerts.ToListAsync();
            int Appcount = Appointment.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)Appcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<VaccinationAlerts> appointments = await db.Vaccination_Alerts.Skip(start).Take(perpage).ToListAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(await db.Vaccination_Alerts.Skip(start).Take(perpage).ToListAsync());
            }else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(await db.Vaccination_Alerts.Skip(start).Take(perpage).ToListAsync());

            }

        }
           
        // GET: VaccinationAlerts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vaccinationAlerts = await db.Vaccination_Alerts
                .Include(v => v.Admin)
                .SingleOrDefaultAsync(m => m.VaccinationAlertsid == id);
            if (vaccinationAlerts == null)
            {
                return NotFound();
            }

            return View(vaccinationAlerts);
        }

        // GET: VaccinationAlerts/Create
        public ActionResult Create()
        {
            return View(db.Vaccination_Alerts.ToList());
        }
        // POST: VaccinationAlerts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create(string vacname, string vacaddress, string vacage, string vacnumber)
        {
            //first thing we do, make sure that we can actually catch the baseball
            //test that by printing out the different fields
          

            string query = "insert into Vaccination_Alerts (name, address, age, vaccinationnumber,adminid) values (@p1, @p2, @p3, @p4,@p5)";
            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("p1", vacname);
            myparams[1] = new SqlParameter("p2", vacaddress);
            myparams[2] = new SqlParameter("p3",vacage);
            myparams[3] = new SqlParameter("p4", vacnumber);
            myparams[4] = new SqlParameter("p5", 1); //change this so that it's the current logged in admin

            db.Database.ExecuteSqlCommand(query, myparams);

            //then you want to connect to the database
            //throw the baseball to the other database by inserting a new record
            return View();
        }

        // GET: VaccinationAlerts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vaccinationAlerts = await db.Vaccination_Alerts.SingleOrDefaultAsync(m => m.VaccinationAlertsid == id);
            if (vaccinationAlerts == null)
            {
                return NotFound();
            }

            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "FirstName", vaccinationAlerts.AdminID);
            return View(vaccinationAlerts);
        }

        // POST: VaccinationAlerts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("VaccinationAlertsid,Name,Address,Age,VaccinationNumber,AdminID")] VaccinationAlerts vaccinationAlerts)
        {
            if (id != vaccinationAlerts.VaccinationAlertsid)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Update(vaccinationAlerts);
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VaccinationAlertsExists(vaccinationAlerts.VaccinationAlertsid))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "AdminFName", vaccinationAlerts);
            return View(vaccinationAlerts);
        }

        // GET: VaccinationAlerts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vaccinationAlerts = await db.Vaccination_Alerts
                .Include(v => v.Admin)
                .SingleOrDefaultAsync(m => m.VaccinationAlertsid == id);
            if (vaccinationAlerts == null)
            {
                return NotFound();
            }

            return View(vaccinationAlerts);
        }

        // POST: VaccinationAlerts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var vaccinationAlerts = await db.Vaccination_Alerts.SingleOrDefaultAsync(m => m.VaccinationAlertsid == id);
            db.Vaccination_Alerts.Remove(vaccinationAlerts);
            await db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VaccinationAlertsExists(int id)
        {
            return db.Vaccination_Alerts.Any(e => e.VaccinationAlertsid == id);
        }
    }
}

