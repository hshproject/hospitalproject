﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using HospitalProject.Models;
using HospitalProject.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace HospitalProject.Controllers
{
    public class FindSpecialistController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public FindSpecialistController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        public async Task<ActionResult> Index(int pagenum)
        {
            // referenced Christine blog-aspcore to create pagination
            var _specialists = await db.FindSpecialists.ToListAsync();
            //var _specialists = db.FindSpecialists.ToList();
            int specialistcount = _specialists.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)specialistcount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] = (pagenum + 1).ToString() + " of " + (maxpage + 1).ToString();

            }
            List<FindSpecialist> findSpecialists = await db.FindSpecialists.Skip(start).Take(perpage).ToListAsync();


            var user = await GetCurrentUserAsync();
            if(user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(await db.FindSpecialists.ToListAsync());
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(await db.FindSpecialists.ToListAsync());
            }
            //return RedirectToAction("List");
        }

        /*public async Task<ActionResult> List(int pagenum)
        {
            var _specialists = await db.FindSpecialists.ToListAsync();
            //var _specialists = db.FindSpecialists.ToList();
            int specialistcount = _specialists.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)specialistcount/perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if ( maxpage > 0 )
            {
                ViewData["PaginationSummary"] = (pagenum + 1).ToString() + " of " + (maxpage + 1).ToString();

            }
            List<FindSpecialist> findSpecialists = await db.FindSpecialists.Skip(start).Take(perpage).ToListAsync();

            var user = await GetCurrentUserAsync();
            if(user != null)
            {
                if(user.AdminID == null) { ViewData["UserHasAdmin"] = "Flase"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(await db.FindSpecialists.ToListAsync());
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(await db.FindSpecialists.ToListAsync());
            }
            //return View(findSpecialists);
        }*/

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("SpecialistFName, SpecialistLName, Phone, Gender, Speciality, AvailableDay, AvailableTime")] FindSpecialist findSpecialist)
        {
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); // forbid becasue no user is logged in
            if (user.AdminID == null) return Forbid(); // because admin account is not logged in
            if (ModelState.IsValid)
            {
                findSpecialist.AdminID = user.AdminID.GetValueOrDefault();
                findSpecialist.Admin = user.Admin;
                db.FindSpecialists.Add(findSpecialist);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
            /*return View(findSpecialist);*/
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind("AdminID,SpecialistID,SpecialistFName,SpecialistLName,Phone,Gender,Speciality,AvailableDay,AvailableTime")] FindSpecialist findSpecialist)
        {
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid();
            if(user.AdminID != findSpecialist.AdminID)
            {
                return Forbid();
            }
            if (ModelState.IsValid)
            {
                db.Entry(findSpecialist).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(findSpecialist);
        }


        public async Task<ActionResult> Edit(int? id, int? adminid)
        {
            if( id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }
            FindSpecialist findSpecialist = db.FindSpecialists.Find(id);
            if ( findSpecialist == null)
            {
                return NotFound();
            }

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); // forbid because no user logged in
            if (user.AdminID != adminid)
            {
                return Forbid(); // forbid because they have the wrong account
            }
            return View(findSpecialist);
        }

        public async Task<ActionResult> Details(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }
            FindSpecialist findSpecialist = db.FindSpecialists.Find(id);
            if (findSpecialist == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid();
            if(user.AdminID != adminid)
            {
                return Forbid();
            }
            return View(findSpecialist);
        }

        public async Task<ActionResult> Delete(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }
            FindSpecialist findSpecialist = db.FindSpecialists.Find(id);
            if (findSpecialist == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid();
            if (user.AdminID != adminid)
            {
                return Forbid();
            }
            return View(findSpecialist);
        }

        /* This action is to delete a country from the Country Table after pressing submit button
         and return to Country/Index
        */
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id, int adminid)
        {
            var user = await GetCurrentUserAsync();
            if(user.AdminID !=adminid)
            {
                return Forbid();
            }
            FindSpecialist findSpecialist = db.FindSpecialists.Find(id);
            db.FindSpecialists.Remove(findSpecialist);
            db.SaveChanges();
            return RedirectToAction("Index"); // To go back to Country/Index 

        }

    }
}