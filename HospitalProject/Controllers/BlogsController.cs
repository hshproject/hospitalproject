﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Models.ViewModels;
using HospitalProject.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Net;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IO;




namespace HospitalProject.Controllers
{
    public class BlogsController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public BlogsController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        // GET: Blogs
        //public async Task<IActionResult> Index()
        //{
        //    var hospitalDbContext = db.BLog.Include(b => b.Admin);
        //    return View(await hospitalDbContext.ToListAsync());
        //}

        // GET: Blogs/Details/5
        public async Task<ActionResult> New()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }

            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }
            return View(db.BLog.ToList());
        }


        // GET: Blogs/Create
        public IActionResult Create()
        {
            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "FirstName");
            return View();
        }

        public async Task<ActionResult> Index(int pageNum)
        {
            var user = await GetCurrentUserAsync();

            // From Christine's Pagination Notes
            var BLogs = await db.BLog.Include(d => d.Admin).ToListAsync();
            int BLogCount = BLogs.Count();
            int perPage = 5;
            int maxPage = (int)Math.Ceiling((decimal)BLogCount / perPage) - 1;
            if (maxPage < 0) maxPage = 0;
            if (pageNum < 0) pageNum = 0;
            if (pageNum > maxPage) pageNum = maxPage;
            int start = perPage * pageNum;

            ViewData["pageNum"] = (int)pageNum;
            ViewData["paginationSummary"] = "";

            if (maxPage > 0)
            {
                ViewData["paginationSummary"] = (pageNum + 1).ToString() + " of " + (maxPage + 1).ToString();
            }
            List<Blogs> appointments = await db.BLog.Skip(start).Take(perPage).ToListAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(await db.BLog.Skip(start).Take(perPage).ToListAsync());
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(await db.BLog.Skip(start).Take(perPage).ToListAsync());

            }

        }


        // POST: Blogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Blogid,Title,Description,AdminID")] Blogs blogs)
        {
            if (ModelState.IsValid)
            {
                db.Add(blogs);
                await db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "FirstName", blogs.AdminID);
            return View(blogs);
        }

        // GET: Blogs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var blogs = await db.BLog.SingleOrDefaultAsync(m => m.Blogid == id);
            if (blogs == null)
            {
                return NotFound();
            }
            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "FirstName", blogs.AdminID);
            return View(blogs);
        }

        // POST: Blogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Blogid,Title,Description,AdminID")] Blogs blogs)
        {
            if (id != blogs.Blogid)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Update(blogs);
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BlogsExists(blogs.Blogid))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "FirstName", blogs.AdminID);
            return View(blogs);
        }

        // GET: Blogs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var blogs = await db.BLog
                .Include(b => b.Admin)
                .SingleOrDefaultAsync(m => m.Blogid == id);
            if (blogs == null)
            {
                return NotFound();
            }

            return View(blogs);
        }

        // POST: Blogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var blogs = await db.BLog.SingleOrDefaultAsync(m => m.Blogid == id);
            db.BLog.Remove(blogs);
            await db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BlogsExists(int id)
        {
            return db.BLog.Any(e => e.Blogid == id);
        }
    }
}

