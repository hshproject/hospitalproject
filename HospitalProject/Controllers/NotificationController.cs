﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Data;
using HospitalProject.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;

namespace HospitalProject.Controllers
{
    public class NotificationController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public NotificationController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        // GET: Notification
        public async Task<IActionResult> Index(int pageNum)
        {
            var user = await GetCurrentUserAsync();
            // From Christine's Pagination Notes
            var _notifications = await db.Notifications.ToListAsync();
            int pageCount = _notifications.Count();
            int perPage = 5;
            int maxPage = (int)Math.Ceiling((decimal)pageCount / perPage) - 1;
            if (maxPage < 0) maxPage = 0;
            if (pageNum < 0) pageNum = 0;
            if (pageNum > maxPage) pageNum = maxPage;
            int start = perPage * pageNum;

            ViewData["pageNum"] = (int)pageNum;
            ViewData["paginationSummary"] = "";

            if (maxPage > 0)
            {
                ViewData["paginationSummary"] = (pageNum + 1).ToString() + " of " + (maxPage + 1).ToString();
            }
            List<Notification> notifications = await db.Notifications.Skip(start).Take(perPage).ToListAsync();


            if (user != null)
            {
                if (user.AdminID == null)
                {
                    ViewData["UserHasAdmin"] = "False";
                }
                else
                {
                    ViewData["UserHasAdmin"] = user.AdminID.ToString();
                }
                return View(notifications);
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(notifications);
            }
        }

        // GET: Notification/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notification = await db.Notifications
                .Include(n => n.Admin)
                .SingleOrDefaultAsync(m => m.NotificationID == id);
            if (notification == null)
            {
                return NotFound();
            }

            return View(notification);
        }

        // GET: Notification/Create
        public IActionResult Create()
        {
            Notification notification = new Notification();
            notification.NotificationCreatedDate = DateTime.Today;
            notification.NotificationUpdatedDate = DateTime.Today;
            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "FirstName");
            return View(notification);
        }

        // POST: Notification/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("NotificationID,NotificationTitle,NotificationText,NotificationStartDate,NotificationEndDate,NotificationCreatedDate,NotificationUpdatedDate,IsPublished,IsDeleted,AdminID")] Notification notification)
        {
            if (ModelState.IsValid)
            {
                db.Add(notification);
                await db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "FirstName", notification.AdminID);
            return View(notification);
        }

        // GET: Notification/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notification = await db.Notifications.SingleOrDefaultAsync(m => m.NotificationID == id);
            if (notification == null)
            {
                return NotFound();
            }
            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "FirstName", notification.AdminID);
            return View(notification);
        }

        // POST: Notification/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("NotificationID,NotificationTitle,NotificationText,NotificationStartDate,NotificationEndDate,NotificationCreatedDate,NotificationUpdatedDate,IsPublished,IsDeleted,AdminID")] Notification notification)
        {
            if (id != notification.NotificationID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Update(notification);
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NotificationExists(notification.NotificationID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "FirstName", notification.AdminID);
            return View(notification);
        }

        // GET: Notification/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notification = await db.Notifications
                .Include(n => n.Admin)
                .SingleOrDefaultAsync(m => m.NotificationID == id);
            if (notification == null)
            {
                return NotFound();
            }

            return View(notification);
        }

        // POST: Notification/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var notification = await db.Notifications.SingleOrDefaultAsync(m => m.NotificationID == id);
            db.Notifications.Remove(notification);
            await db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NotificationExists(int id)
        {
            return db.Notifications.Any(e => e.NotificationID == id);
        }
    }
}
