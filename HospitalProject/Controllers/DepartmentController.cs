﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Data.SqlClient;

// Based off Christine's Example

namespace HospitalProject.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public DepartmentController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        // Returns the list view for Departments
        public async Task<ActionResult> Index(int pageNum)
        {
            var user = await GetCurrentUserAsync();

            // From Christine's Pagination Notes
            var _departments = await db.Departments.Include(d => d.Admin).ToListAsync();
            int departmentCount = _departments.Count();
            int perPage = 5;
            int maxPage = (int)Math.Ceiling((decimal)departmentCount / perPage) - 1;
            if (maxPage < 0) maxPage = 0;
            if (pageNum < 0) pageNum = 0;
            if (pageNum > maxPage) pageNum = maxPage;
            int start = perPage * pageNum;

            ViewData["pageNum"] = (int)pageNum;
            ViewData["paginationSummary"] = "";

            if (maxPage > 0)
            {
                ViewData["paginationSummary"] = (pageNum + 1).ToString() + " of " + (maxPage + 1).ToString();
            }

            List<Department> departments = await db.Departments.Include(d => d.Admin).Skip(start).Take(perPage).ToListAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            return View(departments);
        }

        // Return the Create View for Departments
        public ActionResult Create()
        {
            return View();
        }

        // Runs after the form to create a department is submitted
        // Add the entry to the departments table and return the list view
        [HttpPost]
        public async Task<ActionResult> Create(string department_name, string department_desc)
        {
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID == null) return Forbid(); //forbid because no admin account

            string queryString = "INSERT INTO departments (Name, Description, AdminID) VALUES (@name, @desc, @adminid)";

            SqlParameter[] queryParams = new SqlParameter[3];
            queryParams[0] = new SqlParameter("@name", department_name);
            queryParams[1] = new SqlParameter("@desc", department_desc);
            queryParams[2] = new SqlParameter("@adminid", user.AdminID);

            db.Database.ExecuteSqlCommand(queryString, queryParams);

            return RedirectToAction("Index");
        }

        // Return the edit view for a particular department
        public async Task<ActionResult> Edit(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return NotFound();
            }

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they have no account
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they have the wrong account
            }
            return View(department);
        }

        // Runs after the form to edit a department is submitted
        // Update the entry in the deparments table and return the list view
        [HttpPost]
        public async Task<ActionResult> Edit(int? id, string department_name, string department_desc)
        {
            if ((id == null) || (db.Departments.Find(id) == null))
            {
                return new StatusCodeResult(400);
            }

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID == null) return Forbid(); //forbid because no admin account

            string queryString = "UPDATE departments SET Name=@name, Description=@desc, AdminID=@adminid WHERE DepartmentID=@departmentid";

            SqlParameter[] queryParams = new SqlParameter[4];
            queryParams[0] = new SqlParameter("@name", department_name);
            queryParams[1] = new SqlParameter("@desc", department_desc);
            queryParams[2] = new SqlParameter("@adminid", user.AdminID);
            queryParams[3] = new SqlParameter("@departmentid", id);

            db.Database.ExecuteSqlCommand(queryString, queryParams);

            return RedirectToAction("Index");
        }

        // Return the Delete view for Departments
        public async Task<ActionResult> Delete(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }

            Department department = await db.Departments.SingleOrDefaultAsync(d => d.DepartmentID == id);
            if (department == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); // forbid because they haven't logged in
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they are are the wrong person
            }
            return View(department);
        }

        // Runs after the Delete is confirmed
        // Manually remove all job postings that are classified under this Department and the Department
        // Return the list view for departments
        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed(int id, int admin_id)
        {
            Department department = await db.Departments.FindAsync(id);
            // Referenced https://stackoverflow.com/questions/23090459/ienumerable-where-and-tolist-what-do-they-really-do
            // Get all JobPostings that have this DepartmentID
            IEnumerable<JobPosting> jpToDelete = db.JobPostings.ToList().Where(jp => jp.DepartmentID.Equals(department.DepartmentID));

            var user = await GetCurrentUserAsync();
            if (user.AdminID != admin_id)
            {
                return Forbid();
            }

            db.JobPostings.RemoveRange(jpToDelete);
            db.Departments.Remove(department);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}