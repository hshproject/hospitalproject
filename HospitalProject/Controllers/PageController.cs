﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Data;
using HospitalProject.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;

namespace HospitalProject.Controllers
{
    public class PageController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public PageController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        // GET: Page
        public async Task<IActionResult> Index(int pageNum)
        {
            var user = await GetCurrentUserAsync();
            // From Christine's Pagination Notes
            var _pages = await db.Pages.Include(e => e.Admin).ToListAsync();
            int pageCount = _pages.Count();
            int perPage = 5;
            int maxPage = (int)Math.Ceiling((decimal)pageCount / perPage) - 1;
            if (maxPage < 0) maxPage = 0;
            if (pageNum < 0) pageNum = 0;
            if (pageNum > maxPage) pageNum = maxPage;
            int start = perPage * pageNum;

            ViewData["pageNum"] = (int)pageNum;
            ViewData["paginationSummary"] = "";

            if (maxPage > 0)
            {
                ViewData["paginationSummary"] = (pageNum + 1).ToString() + " of " + (maxPage + 1).ToString();
            }

            List<Page> pages = await db.Pages.Include(e => e.Admin).Skip(start).Take(perPage).ToListAsync();

            if (user != null)
            {
                if (user.AdminID == null)
                {
                    ViewData["UserHasAdmin"] = "False";
                }
                else
                {
                    ViewData["UserHasAdmin"] = user.AdminID.ToString();
                }
                return View(pages);
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(pages);
            }
        }

        // GET: Page/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var page = await db.Pages
                .Include(p => p.Admin)
                .SingleOrDefaultAsync(m => m.PageID == id);
            if (page == null)
            {
                return NotFound();
            }

            return View(page);
        }

        // GET: Page/Create
        public IActionResult Create()
        {
            Page page = new Page();
            page.PageCreatedDate = DateTime.Today;
            page.PageUpdatedDate = DateTime.Today;
            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "FirstName");
            return View(page);
        }

        // POST: Page/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PageID,PageTitle,PageSubtitle,PageCreatedDate,PageUpdatedDate,PageContent,IsPublished,IsDeleted,AdminID")] Page page)
        {
            if (ModelState.IsValid)
            {
                db.Add(page);
                await db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "FirstName", page.AdminID);
            return View(page);
        }

        // GET: Page/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var page = await db.Pages.SingleOrDefaultAsync(m => m.PageID == id);
            if (page == null)
            {
                return NotFound();
            }
            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "FirstName", page.AdminID);
            return View(page);
        }

        // POST: Page/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PageID,PageTitle,PageSubtitle,PageCreatedDate,PageUpdatedDate,PageContent,IsPublished,IsDeleted,AdminID")] Page page)
        {
            if (id != page.PageID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Update(page);
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PageExists(page.PageID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AdminID"] = new SelectList(db.Admins, "AdminID", "FirstName", page.AdminID);
            return View(page);
        }

        // GET: Page/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var page = await db.Pages
                .Include(p => p.Admin)
                .SingleOrDefaultAsync(m => m.PageID == id);
            if (page == null)
            {
                return NotFound();
            }

            return View(page);
        }

        // POST: Page/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var page = await db.Pages.SingleOrDefaultAsync(m => m.PageID == id);
            db.Pages.Remove(page);
            await db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PageExists(int id)
        {
            return db.Pages.Any(e => e.PageID == id);
        }
    }
}
