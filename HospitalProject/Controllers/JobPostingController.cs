﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Models.ViewModels;
using HospitalProject.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using System.Data.SqlClient;

// Based off Christine's Example

namespace HospitalProject.Controllers
{
    public class JobPostingController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public JobPostingController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        // Returns a List View of Job Postings
        public async Task<ActionResult> Index(int pageNum)
        {
            var user = await GetCurrentUserAsync();

            // From Christine's Pagination Notes
            var _jobPostings = await db.JobPostings.Include(jp => jp.Admin).Include(jp => jp.Department).ToListAsync();

            // If the visitor is not an admin only consider active postings
            if (user == null || user.AdminID == null)
            {
                for (int i = 0; i < _jobPostings.Count(); i++)
                {
                    if (_jobPostings[i].IsActive == false)
                    {
                        _jobPostings.Remove(_jobPostings[i]);
                    }
                }
            }

            int jpCount = _jobPostings.Count();
            int perPage = 5;
            int maxPage = (int)Math.Ceiling((decimal)jpCount / perPage) - 1;
            if (maxPage < 0) maxPage = 0;
            if (pageNum < 0) pageNum = 0;
            if (pageNum > maxPage) pageNum = maxPage;
            int start = perPage * pageNum;

            ViewData["pageNum"] = (int)pageNum;
            ViewData["paginationSummary"] = "";

            if (maxPage > 0)
            {
                ViewData["paginationSummary"] = (pageNum + 1).ToString() + " of " + (maxPage + 1).ToString();
            }

            // If the user is an admin user send all job postings to the view, otherwise send only active job postings
            if (user != null)
            {
                if (user.AdminID == null) {
                    ViewData["UserHasAdmin"] = "False";
                    return View(_jobPostings.Skip(start).Take(perPage));
                }
                else {
                    ViewData["UserHasAdmin"] = user.AdminID.ToString();
                    List<JobPosting> jobPostings = await db.JobPostings.Include(jp => jp.Admin).Skip(start).Take(perPage).ToListAsync();
                    return View(jobPostings);
                }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(_jobPostings.Skip(start).Take(perPage));
            }
        }

        // Returns a Detail View for a Single Job Posting
        public async Task<ActionResult> Details(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }
            JobPosting jobPosting = db.JobPostings.Include(jp => jp.Admin).SingleOrDefault(jp => jp.JobPostingID == id);

            if (jobPosting == null)
            {
                return NotFound();
            }

            var user = await GetCurrentUserAsync();

            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            return View(jobPosting);
        }
        
        // Return the Create View for Job Postings
        public ActionResult Create()
        {
            return View(db.Departments.ToList());
        }

        // After the Create form is submitted
        // Insert a new entry into the JobPostings table and return the list view
        [HttpPost]
        public async Task<ActionResult> Create(string job_title, string job_desc, string job_qual, 
            string job_deadline_date, string job_deadline_time, string job_is_active, string job_department)
        {
            string deadline = job_deadline_date + " " + job_deadline_time;
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID == null) return Forbid(); //forbid because no admin account

            string queryString = "INSERT INTO jobpostings (Title, Description, Qualifications, DatePosted, Deadline, IsActive, DepartmentID, AdminID) " +
                "VALUES (@title, @desc, @qual, @postdate, @deadline, @active, @deptid, @adminid)";

            SqlParameter[] queryParams = new SqlParameter[8];
            queryParams[0] = new SqlParameter("@title", job_title);
            queryParams[1] = new SqlParameter("@desc", job_desc);
            queryParams[2] = new SqlParameter("@qual", job_qual);
            queryParams[3] = new SqlParameter("@postdate", DateTime.Now);
            queryParams[4] = new SqlParameter("@deadline", DateTime.Parse(deadline));
            // If the active checkbox is checked set @active to true, otherwise false
            queryParams[5] = job_is_active != null ? new SqlParameter("@active", true) : new SqlParameter("@active", false);
            queryParams[6] = new SqlParameter("@deptid", int.Parse(job_department));
            queryParams[7] = new SqlParameter("@adminid", user.AdminID);

            db.Database.ExecuteSqlCommand(queryString, queryParams);

            return RedirectToAction("Index");
        }

        // Return the Edit View for a particular Job Posting
        public async Task<ActionResult> Edit(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }
            JobPosting jobPosting = db.JobPostings.Find(id);
            JobPostingEdit jpEditView = new JobPostingEdit();

            if (jobPosting== null)
            {
                return NotFound();
            }

            jpEditView.JobPosting = jobPosting;
            jpEditView.Departments = db.Departments.ToList();

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they have no account
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they have the wrong account
            }
            return View(jpEditView);
        }

        // After the Edit Form is Submitted
        // Edit an existing entry in the JobPostings Table and return the Details view for that particular posting
        [HttpPost]
        public async Task<ActionResult> Edit(int? id, string job_title, string job_desc, string job_qual,
            string job_deadline_date, string job_deadline_time, string job_is_active, string job_department)
        {
            if ((id == null) || (db.JobPostings.Find(id) == null))
            {
                return new StatusCodeResult(400);
            }

            string deadline = job_deadline_date + " " + job_deadline_time;
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID == null) return Forbid(); //forbid because no admin account

            string queryString = "UPDATE jobpostings SET Title=@title, Description=@desc, Qualifications=@qual, " +
                "Deadline=@deadline, IsActive=@active, DepartmentID=@deptid, AdminID=@adminid WHERE JobPostingID=@jpid";

            SqlParameter[] queryParams = new SqlParameter[8];
            queryParams[0] = new SqlParameter("@title", job_title);
            queryParams[1] = new SqlParameter("@desc", job_desc);
            queryParams[2] = new SqlParameter("@qual", job_qual);
            queryParams[3] = new SqlParameter("@deadline", DateTime.Parse(deadline));
            queryParams[4] = job_is_active != null ? new SqlParameter("@active", true) : new SqlParameter("@active", false);
            queryParams[5] = new SqlParameter("@deptid", int.Parse(job_department));
            queryParams[6] = new SqlParameter("@adminid", user.AdminID);
            queryParams[7] = new SqlParameter("@jpid", id);

            db.Database.ExecuteSqlCommand(queryString, queryParams);

            return RedirectToAction("Details", new { id, adminid = user.AdminID });
        }

        // Return the Delete View for a particular Job Posting
        public async Task<ActionResult> Delete(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }

            JobPosting jobPosting = await db.JobPostings.Include(jp => jp.Department).SingleOrDefaultAsync(jp => jp.JobPostingID == id);
            if (jobPosting == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); // forbid because they haven't logged in
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they are are the wrong person
            }
            return View(jobPosting);
        }

        // After the delete is confirmed
        // Remove the particular entry from the JobPostings table
        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed(int id, int admin_id)
        {
            JobPosting jobPosting = await db.JobPostings.FindAsync(id);

            var user = await GetCurrentUserAsync();
            if (user.AdminID != admin_id)
            {
                return Forbid();
            }

            db.JobPostings.Remove(jobPosting);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}