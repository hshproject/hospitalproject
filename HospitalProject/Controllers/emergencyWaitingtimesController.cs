﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Models.ViewModels;
using HospitalProject.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Net;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IO;


namespace HospitalProject.Controllers
{
    public class emergencyWaitingtimesController : Controller
    {
        private readonly HospitalDbContext _context;

        public emergencyWaitingtimesController(HospitalDbContext context)
        {
            _context = context;
        }

        // GET: emergencyWaitingtimes
        public async Task<IActionResult> Index()
        {
            var hospitalDbContext = _context.emergency_Waitingtime.Include(e => e.Admin);
            return View(await hospitalDbContext.ToListAsync());
        }

        // GET: emergencyWaitingtimes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var emergencyWaitingtimes = await _context.emergency_Waitingtime
                .Include(e => e.Admin)
                .SingleOrDefaultAsync(m => m.PatientID == id);
            if (emergencyWaitingtimes == null)
            {
                return NotFound();
            }

            return View(emergencyWaitingtimes);
        }

        // GET: emergencyWaitingtimes/Create
        public IActionResult Create()
        {
            ViewData["AdminID"] = new SelectList(_context.Admins, "AdminID", "FirstName");
            return View();
        }

        // POST: emergencyWaitingtimes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PatientID,Name,Address,Age,ArrivalTime,DoctorsTime,AdminID")] emergencyWaitingtimes emergencyWaitingtimes)
        {
            if (ModelState.IsValid)
            {
                _context.Add(emergencyWaitingtimes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AdminID"] = new SelectList(_context.Admins, "AdminID", "FirstName", emergencyWaitingtimes.AdminID);
            return View(emergencyWaitingtimes);
        }

        // GET: emergencyWaitingtimes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var emergencyWaitingtimes = await _context.emergency_Waitingtime.SingleOrDefaultAsync(m => m.PatientID == id);
            if (emergencyWaitingtimes == null)
            {
                return NotFound();
            }
            ViewData["AdminID"] = new SelectList(_context.Admins, "AdminID", "FirstName", emergencyWaitingtimes.AdminID);
            return View(emergencyWaitingtimes);
        }

        // POST: emergencyWaitingtimes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PatientID,Name,Address,Age,ArrivalTime,DoctorsTime,AdminID")] emergencyWaitingtimes emergencyWaitingtimes)
        {
            if (id != emergencyWaitingtimes.PatientID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(emergencyWaitingtimes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!emergencyWaitingtimesExists(emergencyWaitingtimes.PatientID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AdminID"] = new SelectList(_context.Admins, "AdminID", "FirstName", emergencyWaitingtimes.AdminID);
            return View(emergencyWaitingtimes);
        }

        // GET: emergencyWaitingtimes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var emergencyWaitingtimes = await _context.emergency_Waitingtime
                .Include(e => e.Admin)
                .SingleOrDefaultAsync(m => m.PatientID == id);
            if (emergencyWaitingtimes == null)
            {
                return NotFound();
            }

            return View(emergencyWaitingtimes);
        }

        // POST: emergencyWaitingtimes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var emergencyWaitingtimes = await _context.emergency_Waitingtime.SingleOrDefaultAsync(m => m.PatientID == id);
            _context.emergency_Waitingtime.Remove(emergencyWaitingtimes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool emergencyWaitingtimesExists(int id)
        {
            return _context.emergency_Waitingtime.Any(e => e.PatientID == id);
        }
    }
}
