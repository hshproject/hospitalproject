﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HospitalProject.Migrations
{
    public partial class BirinderNotificationModal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PatientProfiles_Departments_DepartmentID",
                table: "PatientProfiles");

            migrationBuilder.CreateTable(
                name: "Blog",
                columns: table => new
                {
                    Blogid = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AdminID = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: false),
                    Title = table.Column<string>(maxLength: 500, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blog", x => x.Blogid);
                    table.ForeignKey(
                        name: "FK_Blog_Admins_AdminID",
                        column: x => x.AdminID,
                        principalTable: "Admins",
                        principalColumn: "AdminID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "emergency_Watiningtime",
                columns: table => new
                {
                    PatientID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(nullable: false),
                    AdminID = table.Column<int>(nullable: false),
                    Age = table.Column<string>(nullable: false),
                    ArrivalTime = table.Column<string>(nullable: false),
                    DoctorsTime = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_emergency_Watiningtime", x => x.PatientID);
                    table.ForeignKey(
                        name: "FK_emergency_Watiningtime_Admins_AdminID",
                        column: x => x.AdminID,
                        principalTable: "Admins",
                        principalColumn: "AdminID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Newsletter",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    date_created = table.Column<DateTime>(maxLength: 255, nullable: false),
                    name = table.Column<string>(maxLength: 255, nullable: false),
                    post_details = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Newsletter", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "NewsletterSubscribers",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    email = table.Column<string>(maxLength: 255, nullable: false),
                    name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsletterSubscribers", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Vaccination_Alerts",
                columns: table => new
                {
                    VaccinationAlertsid = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(nullable: false),
                    AdminID = table.Column<int>(nullable: false),
                    Age = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    VaccinationNumber = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vaccination_Alerts", x => x.VaccinationAlertsid);
                    table.ForeignKey(
                        name: "FK_Vaccination_Alerts_Admins_AdminID",
                        column: x => x.AdminID,
                        principalTable: "Admins",
                        principalColumn: "AdminID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Blog_AdminID",
                table: "Blog",
                column: "AdminID");

            migrationBuilder.CreateIndex(
                name: "IX_emergency_Watiningtime_AdminID",
                table: "emergency_Watiningtime",
                column: "AdminID");

            migrationBuilder.CreateIndex(
                name: "IX_Vaccination_Alerts_AdminID",
                table: "Vaccination_Alerts",
                column: "AdminID");

            migrationBuilder.AddForeignKey(
                name: "FK_PatientProfiles_Departments_DepartmentID",
                table: "PatientProfiles",
                column: "DepartmentID",
                principalTable: "Departments",
                principalColumn: "DepartmentID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PatientProfiles_Departments_DepartmentID",
                table: "PatientProfiles");

            migrationBuilder.DropTable(
                name: "Blog");

            migrationBuilder.DropTable(
                name: "emergency_Watiningtime");

            migrationBuilder.DropTable(
                name: "Newsletter");

            migrationBuilder.DropTable(
                name: "NewsletterSubscribers");

            migrationBuilder.DropTable(
                name: "Vaccination_Alerts");

            migrationBuilder.AddForeignKey(
                name: "FK_PatientProfiles_Departments_DepartmentID",
                table: "PatientProfiles",
                column: "DepartmentID",
                principalTable: "Departments",
                principalColumn: "DepartmentID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
