﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

using HospitalProject.Models;

// Based off Christine's Example

namespace HospitalProject.Data
{
    public class HospitalDbContext : IdentityDbContext<ApplicationUser>
    {
        public HospitalDbContext(DbContextOptions<HospitalDbContext> options)
        : base(options)
        {

        }

        public DbSet<Admin> Admins { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Faq> Faqs { get; set; }
        public DbSet<JobPosting> JobPostings { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Newsletter> Newsletter { get; set; }
        public DbSet<NewsletterSubscribers> NewsletterSubscribers { get; set; }
        public DbSet<Donation> Donations { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Link> Links { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<FindSpecialist> FindSpecialists { get; set; }
        public DbSet<ReachUs> Reach_Us { get; set; }
        public DbSet<PatientProfile> PatientProfiles { get; set; }
        public DbSet<VaccinationAlerts> Vaccination_Alerts { get; set; }
        public DbSet<emergencyWaitingtimes> emergency_Waitingtime { get; set; }
        public DbSet<Blogs> BLog { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JobPosting>()
                .HasOne(jp => jp.Department)
                .WithMany(d => d.JobPostings)
                .HasForeignKey(jp => jp.DepartmentID);

            modelBuilder.Entity<JobPosting>() //this is talking about jobpostings
                .HasOne(jp => jp.Admin) //job posting has one admin, but now we are talking about admins
                .WithMany(a => a.JobPostings)//admin has many jobpostings, but now we are talking about jobpostings again
                .HasForeignKey(jp => jp.AdminID);//a job posting has an admin id

            modelBuilder.Entity<FindSpecialist>()
                .HasOne(a => a.Admin)
                .WithMany(f => f.FindSpecialists)
                .HasForeignKey(a => a.AdminID);

            modelBuilder.Entity<Department>()
                .HasOne(d => d.Admin)
                .WithMany(a => a.Departments)
                .HasForeignKey(d => d.AdminID);

            modelBuilder.Entity<Page>()
                .HasOne(d => d.Admin)
                .WithMany(a => a.Pages)
                .HasForeignKey(d => d.AdminID);

            modelBuilder.Entity<Faq>()
                .HasOne(f => f.Category)
                .WithMany(c => c.Faqs)
                .HasForeignKey(f => f.CategoryID);

            modelBuilder.Entity<Faq>()
                .HasOne(f => f.Admin)
                .WithMany(a => a.Faqs)
                .HasForeignKey(f => f.AdminID);

            modelBuilder.Entity<Newsletter>()
                .HasOne(n => n.Admin)
                .WithMany(a => a.Newsletters)
                .HasForeignKey(n => n.AdminID);

            modelBuilder.Entity<Newsletter>()
             .HasOne(f => f.Admin)
             .WithMany(c => c.Newsletters)
             .Metadata.DeleteBehavior = DeleteBehavior.Restrict;

            modelBuilder.Entity<NewsletterSubscribers>()
                .HasOne(n => n.Admin)
                .WithMany(a => a.NewsletterSubscribers)
                .HasForeignKey(n => n.AdminID);

            modelBuilder.Entity<NewsletterSubscribers>()
              .HasOne(f => f.Admin)
              .WithMany(c => c.NewsletterSubscribers)
              .Metadata.DeleteBehavior = DeleteBehavior.Restrict;

            modelBuilder.Entity<Booking>()
                .HasOne(b => b.Department)
                .WithMany(d => d.Bookings)
                .HasForeignKey(b => b.DepartmentID);

            modelBuilder.Entity<Booking>()
                .HasOne(b => b.Admin)
                .WithMany(a => a.Bookings)
                .HasForeignKey(n => n.AdminID);

            modelBuilder.Entity<Booking>()
               .HasOne(f => f.Admin)
               .WithMany(c => c.Bookings)
               .Metadata.DeleteBehavior = DeleteBehavior.Restrict;

            // https://github.com/aspnet/EntityFrameworkCore/issues/3815
            modelBuilder.Entity<Faq>()
                .HasOne(f => f.Category)
                .WithMany(c => c.Faqs)
                .Metadata.DeleteBehavior = DeleteBehavior.Restrict;

            modelBuilder.Entity<JobPosting>()
                .HasOne(jp => jp.Department)
                .WithMany(d => d.JobPostings)
                .Metadata.DeleteBehavior = DeleteBehavior.Restrict;

            modelBuilder.Entity<Category>()
                .HasOne(c => c.Admin)
                .WithMany(a => a.Categories)
                .HasForeignKey(c => c.AdminID);

            modelBuilder.Entity<Event>()
                .HasOne(e => e.Admin)
                .WithMany(a => a.Events)
                .HasForeignKey(e => e.AdminID);

            modelBuilder.Entity<Admin>()
                .HasOne(a => a.User)
                .WithOne(u => u.Admin)
                .HasForeignKey<ApplicationUser>(u => u.AdminID);

            modelBuilder.Entity<PatientProfile>()
                .HasOne(a => a.Admin)
                .WithMany(p => p.PatientProfiles)
                .HasForeignKey(a => a.AdminID);

            modelBuilder.Entity<PatientProfile>()
                .HasOne(d => d.Department)
                .WithMany(p => p.PatientProfiles)
                .HasForeignKey(d => d.DepartmentID);

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Admin>().ToTable("Admins");
            modelBuilder.Entity<Department>().ToTable("Departments");
            modelBuilder.Entity<JobPosting>().ToTable("JobPostings");
            modelBuilder.Entity<Category>().ToTable("Categories");
            modelBuilder.Entity<Faq>().ToTable("Faqs");
            modelBuilder.Entity<Event>().ToTable("Events");
            modelBuilder.Entity<PatientProfile>().ToTable("PatientProfiles");
            modelBuilder.Entity<VaccinationAlerts>().ToTable("Vaccination_Alerts");
            modelBuilder.Entity<emergencyWaitingtimes>().ToTable("emergency_Watiningtime");
            modelBuilder.Entity<Blogs>().ToTable("Blog");
            modelBuilder.Entity<Booking>().ToTable("Booking");
            modelBuilder.Entity<NewsletterSubscribers>().ToTable("NewsletterSubscribers");
        }
    }
}
