﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class ReachUs
    {
        [Key]
        public int ReachUsID { get; set; }

        [Required, StringLength(50), Display(Name = "First Name")]
        public string FName { get; set; }

        [Required, StringLength(50), Display(Name = "Last Name")]
        public string LName { get; set; }

        /*[Required, Display(Name = "Phone Number")]
        public int Phone { get; set; }*/

        /* https://stackoverflow.com/questions/28904826/phone-number-validation-mvc 
        * I have used stackoverflow to get a way to have a regular expression on the method to validate the phone number
        */
        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "You must provide a phone number")]
        [Display(Name = "Phone Number")]
        [RegularExpression(@"^(\d{10})$", ErrorMessage = "Wrong phone number")]
        public string Phone { get; set; }

        [Required, StringLength(100), Display(Name ="Email")]
        public string Email { get; set; }

        [StringLength(500), Display(Name ="Comment")]
        public string Comment { get; set; }

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }


    }
}
