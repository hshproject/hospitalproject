﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Faq
    {
        [Key]
        public int FaqID { get; set; }

        [Required, StringLength(255), Display(Name = "Question")]
        public string Question { get; set; }

        [Required, StringLength(255), Display(Name = "Answer")]
        public string Answer { get; set; }

        [Required, Display(Name = "Date Added")]
        public DateTime DateAdded { get; set; }

        [ForeignKey("CategoryID")]
        public int CategoryID { get; set; }

        public virtual Category Category { get; set; }

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
