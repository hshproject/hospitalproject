﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class PatientProfile
    {
        [Key]
        public int PatientID { get; set; }

        [Required, StringLength(50), Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required, StringLength(50), Display(Name = "Last Name")]
        public string LastName { get; set; }

        // this is part to have Date type to only show in format dd/mm/yyyy
        // Taken from stackoverflow 
        //https://stackoverflow.com/questions/29665290/validate-age-according-to-date-of-birth-using-model-in-mvc-4
        [Required, Display(Name = "Date of Birth")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DOB { get; set; }

        [Required, StringLength(500), Display(Name = "Notes of Patient")]
        public string Notes { get; set; }

        [ForeignKey("DeparmentID")]
        public int DepartmentID { get; set; }

        public virtual Department Department { get; set; }

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }


    }
}

