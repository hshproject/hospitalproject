﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Event
    {
        [Key]
        public int EventID { get; set; }

        [Required, StringLength(255), Display(Name = "Name")]
        public string Name { get; set; }

        [Required, StringLength(255), Display(Name = "Description")]
        public string Description { get; set; }

        [Required, Display(Name = "Start Time")]
        public DateTime StartTime { get; set; }

        [Required, Display(Name = "End Time")]
        public DateTime EndTime { get; set; }

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
