﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HospitalProject.Models.ViewModels
{
    public class PatientProfileEdit
    {
        public PatientProfileEdit()
        {

        }

        public virtual PatientProfile PatientProfile { get; set; }

        public IEnumerable<Department> Departments { get; set; }
    }
}
