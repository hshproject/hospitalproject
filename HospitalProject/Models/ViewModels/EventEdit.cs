﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HospitalProject.Models.ViewModels
{
    public class EventEdit
    {
        public EventEdit()
        {

        }

        public virtual Event Event { get; set; }

        public IEnumerable<Department> Departments { get; set; }
    }
}
