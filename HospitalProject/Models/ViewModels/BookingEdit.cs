﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HospitalProject.Models.ViewModels
{
    public class BookingEdit
    {
        public BookingEdit()
        {

        }

        public virtual Booking Booking { get; set; }

        public IEnumerable<Department> Departments { get; set; }
    }
}
