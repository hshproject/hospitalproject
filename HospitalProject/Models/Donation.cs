﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Donation
    {
        [Key]
        public int DonationID { get; set; }

        [Required, StringLength(255), Display(Name = "Donor Name")]
        public string DonorName { get; set; }

        [Required, StringLength(255), Display(Name = "Donor Email")]
        public string DonorEmail { get; set; }

        [Required, StringLength(10), Display(Name = "Donor Phone")]
        public string DonorPhone { get; set; }

        [Required, StringLength(255), Display(Name = "Donation Type")]
        public string DonationType { get; set; }

        [Required, Display(Name = "Amount")]
        public float DonationAmount { get; set; }

        [Required, Display(Name = "Donation Status")]
        public int DonationStatus { get; set; }

        [Required, Display(Name = "Donation Timestamp")]
        public DateTime DonationDate { get; set; }
     }
}
