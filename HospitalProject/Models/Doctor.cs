﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Doctor
    {

        [Key]
        public int id { get; set; }

        [Required, StringLength(255), Display(Name = "Doctor Name")]
        public string name { get; set; }

        [Required, StringLength(255), Display(Name = "Speciality")]
        public string Speciality { get; set; }
    }
}
