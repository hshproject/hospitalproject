﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Page
    {
        public Page()
        {
            PageCreatedDate = DateTime.Now;
            PageUpdatedDate = DateTime.Now;
        }

        [Key]
        public int PageID { get; set; }

        [Required, StringLength(255), Display(Name = "Page Title")]
        public string PageTitle { get; set; }

        [Required, StringLength(255), Display(Name = "Page Subtitle")]
        public string PageSubtitle {get; set;}

        [Required, Display(Name = "Created Date")]
        public DateTime PageCreatedDate { get; set; }

        [Required, Display(Name = "Modified Date")]
        public DateTime PageUpdatedDate { get; set; }

        [Required, StringLength(2000), Display(Name = "Page Content")]
        public string PageContent { get; set; }

        [Required, Display(Name = "isPublished")]
        public bool IsPublished { get; set; }

        [Required, Display(Name = "isDeleted")]
        public bool IsDeleted { get; set; }

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
