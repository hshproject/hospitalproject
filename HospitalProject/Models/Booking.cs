﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HospitalProject.Models
{
    [Table("Booking")]
    public class Booking
    {
        [Key]
        public int id { get; set; }

        [Required, Display(Name = "Booking Time")]
        public DateTime time { get; set; }

        [Required, StringLength(255), Display(Name = "Patient Name")]
        public string Patient_name { get; set; }

        [Required, StringLength(255), Display(Name = "Email")]
        public string email { get; set; }

        [Required, Display(Name = "IsActive")]
        public Boolean Appointment_Active { get; set; }        

        [ForeignKey("DepartmentID")]
        public int DepartmentID { get; set; }

        public virtual Department Department { get; set; }

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }

        //   public virtual FindSpecialist Specialist { get; set; }// changed from Doctor

    }
}
